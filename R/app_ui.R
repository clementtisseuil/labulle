#' The application User-Interface
#'
#' @param request Internal parameter for `{shiny}`.
#'     DO NOT REMOVE.
#' @import shiny 
#' @import shinydashboard
# @import rsconnect
#' @noRd
#'



app_ui <- function(request) {


  # golem::use_internal_file(
  #   path = "data-raw",
  #   name = "data-bulle.R",
  #   pkg = golem::get_golem_wd(),
  #   dir = "inst/app/www",
  #   open = FALSE,
  #   dir_create = TRUE
  # )
  # source("data-raw/data-bulle.R")
  data(data_bulle)

  tagList(
    # Leave this function for adding external resources
    golem_add_external_resources(),
    # List the first level UI elements here
    # shinythemes::themeSelector(),
    # titlePanel(title = div(img(src="www/bulle-gif2-bouge.gif",height=100),style="text-align: justify;")),
    # titlePanel(title = HTML('<center><img src="www/bulle-gif2-bouge.gif" width="200"></center>')),
    # tags$head(
    #   HTML('<link rel="icon" href="www/bulle-gif2-bouge.gif" type="image/png" />')),

    fluidPage(
      list(tags$head(HTML('<link rel="icon", href="www/bulle-gif2-bouge.gif",
                                   type="image/gif" />'))),
      div(style="padding: 10px 10px; width: '100%'",
          titlePanel(
            title="", windowTitle="My Window Title"
          )
      ),


      navbarPage(
        theme = "cerulean",
        # title = "Menu",
        # title = HTML('<center><img src="www/bulle-gif2-bouge.gif" width="200"></center>'),
        # title = HTML('<link rel="icon" href="www/bulle-gif2-bouge.gif" type="image/png" />'),
        # title = HTML('<center><img src="www/bulle-gif2-bouge.gif" width="50"></center>'),


        title = div(
          div(
            # id = "img-id",
            img(src = "www/bulle-gif2-bouge.gif"
                ,id = "img-id"
                # ,class="topleft"
                )
          ),
          # "Superzip"
        ),

        # img(src = "www/bulle-gif2-bouge.gif",id = "img-id"),

        # tabPanel("Flux",mod_flux_ui("flux_1")),
        tabPanel("Communes",mod_communes_ui("communes_1")),
        tabPanel("A propos", mod_about_ui("about_1"))
        # tabPanel("Navbar 3", "This panel is intentionally left blank")
      )
    )
  )
}

#' Add external Resources to the Application
#'
#' This function is internally used to add external
#' resources inside the Shiny application.
#'
#' @import shiny
#' @import pkgload
#' @importFrom golem add_resource_path activate_js favicon bundle_resources
#' @noRd
golem_add_external_resources <- function() {
  add_resource_path(
    "www",
    app_sys("app/www")
  )

  tags$head(
    favicon(),
    bundle_resources(
      path = app_sys("app/www"),
      app_title = "shinybulle"
    )
    # Add here other external resources
    # for example, you can add shinyalert::useShinyalert()
  )
}
