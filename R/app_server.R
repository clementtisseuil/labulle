#' The application server-side
#'
#' @param input,output,session Internal parameters for {shiny}.
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_server <- function(input, output, session) {
  # Your application server logic
  # mod_flux_server("flux_1")
  mod_communes_server("communes_1")
  mod_about_server("about_1")
  # mod_ftpdata_server("ftpdata_1")
  # mod_infobox_server("infobox_1")

}


