## code to prepare `data-bulle` dataset goes here


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
## CHARGEMENT DES LIBRAIRIES
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## https://anyone-can-cook.github.io/rclass2/lectures/strings_and_regex/strings_and_regex.html


# Liste des bibliothèques que vous souhaitez installer
libraries <- c("tidyr", "dplyr", "stringr", "readr","lubridate","glue","RCurl","vroom","writexl")

# Vérifier et installer les bibliothèques manquantes
for (lib in libraries) {
  if (!require(lib, character.only = TRUE, quietly = TRUE)) {
    install.packages(lib, dependencies = TRUE)
  } else {
    library(lib,character.only = TRUE)
  }
}
  

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
## FONCTIONS
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## FTP access
url <- "ftp://mail.monnaie-bulle.fr:21"
userpwd <-  "clementtisseuil:4w85byS6AA"

## Get URL access to ftp files
readftp <- function(url=NULL,userpwd = NULL, ftp.use.epsv = TRUE, 
                    ftp.ssl = TRUE, ssl.verifypeer = TRUE, ssl.verifyhost = TRUE) {
  dat <- getURL(
    url=url,
    userpwd = userpwd,
    ftp.use.epsv = ftp.use.epsv,
    ftp.ssl = ftp.ssl,
    ssl.verifypeer = ssl.verifypeer,
    ssl.verifyhost = ssl.verifyhost,
    .encoding = 'UTF-8') 
  return(dat)
}


## Write file to the ftp server
writeftp <- function(what = NULL,to = NULL, userpwd = NULL,ftp.use.epsv = TRUE,
                     ftp.ssl = TRUE, ssl.verifypeer = TRUE, ssl.verifyhost = TRUE) {
  
  ftpUpload(
    what = what,
    to = to,
    userpwd = userpwd,
    ftp.use.epsv = TRUE,
    ftp.ssl = TRUE,
    ssl.verifypeer = TRUE,
    ssl.verifyhost = TRUE)
}

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
## TELECHARGEMENT DES FICHIERS DU FTP 
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

llx_societe <- 
  readftp(url = glue("{url}/llx_societe.csv"),userpwd=userpwd) %>% 
  vroom(col_types="iccccc",locale = vroom::locale(encoding = "UTF-8")) 


llx_societe_extrafields <- 
  readftp(url = glue("{url}/llx_societe_extrafields.csv"),userpwd=userpwd) %>% 
  vroom(locale = vroom::locale(encoding = "UTF-8")) 

accounts <- 
  readftp(url = glue("{url}/accounts.csv"),userpwd=userpwd) %>% 
  vroom(col_types="ic",locale = vroom::locale(encoding = "UTF-8")) 

transactions <- 
  readftp(url = glue("{url}/transactions.csv"),userpwd=userpwd) %>% 
  vroom(locale = vroom::locale(encoding = "UTF-8")) 


# colnames(transactions)
# %>%
#   mutate(mm = month(date),yy=year(date), yearmonth=yy*100+mm)

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
## PREPARATION DES DONNÉES
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

### Base de données des contacts (comptes pro et particuliers)
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

societe <- llx_societe %>%
  full_join(llx_societe_extrafields,by=c("rowid"="fk_object")) %>%
  mutate(categorie="professionnel",
         town = str_to_upper(town)) %>%
  mutate(across(where(is.character), ~ na_if(., "NULL"))) %>%
  filter(!is.na(noadh))

contact <-
  societe %>%
  # select(-id) %>%
  full_join(accounts,by=c("noadh"="username")) %>%
  mutate(categorie = replace_na(categorie,"particulier")) %>% 
  filter(!is.na(id)) %>%
  relocate(id, .before = rowid)

## Croisement des Transactions avec les contacts
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

data_bulle <-
  
  transactions %>%

    ## Correction des caractères spéciaux
  mutate(name=str_to_lower(name), 
         name=str_replace(name,"a compte de debit","à compte de débit"),
         name=str_replace(name,"debit","débit"),
         name=str_replace(name,"a professionnel","à professionnel")
        ) %>% 
  
  ## Type d'enregistrement
  mutate(
    type_enregistrement = case_when(
      name %in% c("paiement de compte de débit à compte organisationnel",
                  "paiement de compte de débit",
                  "débit vers pro")
                  ~ "Création",
      
      name %in% c("paiement à compte de débit",
                  "paiement de compte organisationnel à compte de débit",
                  "paiement de compte organisationnel",
                  "paiement à compte organisationnel",
                  "paiement à compte de l'association"
                  ) ~ "Reconversion",
      
      name %in% c("paiement du professionnel au particulier",
                  "paiement de particulier à professionnel",
                  "paiement entre professionnels",
                  "paiement entre particuliers"
                  ) ~ "Transaction commerciale",
      name %in% c("asso vers pro") ~ "Transaction technique")
  ) %>%
  
  
  ## Identification de l'origine des transactions
  mutate(
    from_categorie2 = case_when(
      str_detect(name,"du pro|entre pro") ~ "professionnel",
      str_detect(name,"de part|entre part") ~ "particulier",
      str_detect(name,"de compte orga") ~ "compte organisationnel",
      str_detect(name,"debit vers|de compte de debit|de compte de débit") ~ "compte de débit",
      str_detect(name,"asso vers") ~ "association",
      TRUE ~ "autre"),
    to_categorie2 = case_when(
      str_detect(name,"vers pro|entre pro|a pro") ~ "professionnel",
      str_detect(name,"vers part|entre part|au part") ~ "particulier",
      str_detect(name,"a compte de debit|à compte de débit") ~ "compte de débit",
      str_detect(name,"à compte orga") ~ "compte organisationnel",
      str_detect(name,"à compte de l'asso") ~ "association",
      TRUE ~ "autre")
    ) %>%
  
  left_join(contact %>% rename_with(~ paste("from",.x,sep="_")),
            by=c("from_id"="from_id")) %>%
  left_join(contact %>% rename_with(~ paste("to",.x,sep="_")),
            by=c("to_id"="to_id")) %>%
  mutate(yy=year(date), 
         mm = month(date),
         dd=day(date), 
         yearmonth=yy*100+mm,
         date2 = ymd(paste(yy,mm,dd,sep="-")),
         date2P1 = date2+1,
         id_trans = c(1:nrow(.))) %>%
  
  ## Identification des régies  
  mutate(
    from_regie = case_when(
      str_detect(from_noadh,"8572601|8572602|869421") ~ 1,
      TRUE ~ 0),
    to_regie = case_when(
      str_detect(to_noadh,"8572601|8572602|869421") ~ 1,
      TRUE ~ 0)
  ) %>%
  
  as_tibble()


# glimpse(data_bulle)

## Filtre des données de transaction 
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

# -------------------------------------
# 1. Suppression des Chargeback
# -------------------------------------
#Supprimer les lignes dont la colonne "subclass" = "CHARGEBACK" ET la transaction équivalente 
#(même montant, date peu éloignée, inversement des From to) Ce sont des transactions annulées.


chargeback <- 
  data_bulle %>% 
  filter(grepl("CHARGEBACK",subclass))

chargeback_mirror <- 
  inner_join(x=data_bulle, 
             y=chargeback, 
             by = join_by(from_id==to_id,
                          to_id==from_id,
                          amount==amount,
                          date2 <= date2,
                          date2P1 >= date2)) 

rm_chargeback <- c(chargeback %>% pull(id_trans),
                   chargeback_mirror %>% pull(id_trans.x))

# -------------------------------------
# 2. Supprimer les lignes dont la colonne "replace" contient certains libellés
# -------------------------------------

rm_replace <- 
data_bulle %>%
  filter(str_detect(replace,pattern=c("Toto|Tata|Toyo|Test|test|Essai|Eqssai"))) %>%
  pull(id_trans)


# -------------------------------------
# 3. Supprimer les lignes dont la colonne "date" est strictement inférieure à 01/01/2021.
# -------------------------------------
# On ne garde que les données > 2021

rm_yy <- 
  data_bulle %>%
  filter(yy < 2021) %>%
  pull(id_trans)


# -------------------------------------
# 5. Exclusion des petits montants 
# -------------------------------------

# Supprimer les lignes où il n'y a écrit ni "Test" ni "toto" et dont le montant est 0.1 ou 0.2 ou 1 
# et qui sont au nom d'Aurélien Bisotti (soit en expéditeur, soit en destinataire). 
# Elles sont toutes réalisées entre 2021 et janvier 2022.

rm_amount <- 
data_bulle %>%
  filter(amount <= 1 
         & (str_detect(from_nom,"BISOTTI") | str_detect(to_nom,"BISOTTI")))  %>%
  pull(id_trans)


# -------------------------------------
# 6. Exclusion des transaction vers le compte de réserve "0000"
# -------------------------------------
# Supprimer les lignes dont le montant est supérieur à 300€ (Col. "amount") et qui sont envoyés vers le compte de réserve "0000" 
# (quelle colonne?). Transactions de reconversion.

rm_0000 <- 
data_bulle %>% 
  filter(
    str_starts(to_noadh,"000000"),
    amount >= 300) %>%
  pull(id_trans)
  
# -------------------------------------
# 8 . Supprimer les lignes dont le nom du compte de destination contient "réserve". 
# -------------------------------------
# Supprimer les lignes dont le nom du compte de destination contient "réserve". 
# Transactions CE vers salariés. On le voit dans "replace" (commentaires) mais pas dans le libellé du compte.


rm_reserve <- 
data_bulle %>%
  filter(str_detect(replace,"réserve|reserve|Réserve|Reserve")) %>%
  pull(id_trans)


# -------------------------------------
# Finalisation des données 
# -------------------------------------

rm_id <- unique(c(rm_chargeback,rm_replace,rm_yy,rm_reserve,rm_0000,rm_amount))

data_bulle2 <- 
data_bulle %>%
  mutate(
    include = case_when(id_trans %in% rm_id ~ 0,
                        .default = 1)) %>%
  select(-date2P1)


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
## SAUVEGARDE DES FICHIERS ET TELEVERSEMENT VERS LE FTP 
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


write_xlsx(data_bulle2,path=glue("{tempdir()}/bulles_transactions.xlsx"))
write_xlsx(contact,path=glue("{tempdir()}/bulles_contacts.xlsx"))

writeftp(what = glue("{tempdir()}/bulles_transactions.xlsx"),to = glue("{url}/bulles_transactions.xlsx"), userpwd = userpwd)
writeftp(what = glue("{tempdir()}/bulles_contacts.xlsx"),to = glue("{url}/bulles_contacts.xlsx"), userpwd = userpwd)

# usethis::use_data(data_bulle, overwrite = TRUE)
# usethis::use_data(contact, overwrite = TRUE)

