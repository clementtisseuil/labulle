## Technologies utilisées poru cette application


+ La librarie [`{golem}`](https://github.com/ThinkR-open/golem) a été utilisée pour construire le squelette de l'application. 

+ L'application dans son ensemble se base sur le [`{shiny}`](https://github.com/rstudio/shiny).

+ La visualisation graphique utilise [`{ggplot2}`](https://github.com/tidyverse/ggplot2)

+ L'application est déployée sur [Shiny Apps](https://shinyapps.io.com)

Pour plus d'information sur le code source :  https://gitlab.com/clementtisseuil/labulle
