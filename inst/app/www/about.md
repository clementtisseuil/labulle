## A propos de l'application

Cette application a été développé par [Clement Tisseuil](https://tisseuil.netlify.app/) dans de le cadre u développement de la monnaie locale charentaise, [la Bulle](https://monnaie-bulle.fr/). 

L'application permets de suivre l'évolution des flux monétaires entre particuliers et professionnels au sein du réseau. 

### Plus d'information sur la monnaie locae charentaise : 
- Site web [La Bulle](https://monnaie-bulle.fr/)
- [Gitlab](https://gitlab.com/clementtisseuil/labulle.git)
- L'application [shinybulle] (https://labulle.shinyapps.io/LaBulle/)



